# CoderDojo Social Media

These images were originally created by the CoderDojo Foundation. The images were only available in the EPS format and have been converted to the SVG format.

Fonts used:
* Libre Franklin Heavy (https://www.1001fonts.com/libre-franklin-font.html)
* Lato Heavy (https://www.1001fonts.com/lato-font.html)

